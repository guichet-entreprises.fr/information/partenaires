﻿<!-- include-file(gp.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     _____           _                   _               
|   / ____|     (_)    | |        | |   |  __ \         | |                 (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__) |_ _ _ __| |_ ___ _ __   __ _ _ _ __ ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  ___/ _` | '__| __/ _ \ '_ \ / _` | | '__/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |  | (_| | |  | ||  __/ | | | (_| | | | |  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |_|   \__,_|_|   \__\___|_| |_|\__,_|_|_|  \___||___/
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:title)="Qui sommes-nous ?" -->

Qui sommes-nous ?
================

Le site Guichet Partenaires, un site de l'Institut national de la propriété industrielle
------------------------------------------------------------------------------------------

L'Institut national de la propriété industrielle (INPI) a pour mission d’assurer la mise à disposition d’un service électronique accessible par l’Internet, sécurisé et gratuit, permettant :
* d’accomplir, à distance et par voie électronique, les formalités nécessaires à la création, aux modifications de situation et à la cessation d’activité d’une entreprise ainsi qu’à l’accès à une activité réglementée et à son exercice, au sens de la [directive 2006/123/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32006L0123) du Parlement européen et du Conseil du 12 décembre 2006 relative aux services dans le marché intérieur (« directive services »), mentionnées aux articles [R. 123-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000021926763) et [R. 123-30-2](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000032941383&dateTexte=&categorieLien=cid) du Code de commerce ;
* d’accomplir, à distance et par voie électronique, les formalités, procédures et exigences en matière de reconnaissance, pour l’exercice d’une profession réglementée en France, des qualifications professionnelles acquises dans un autre Etat membre de l’Union européenne ou de l’Espace économique européen, au sens de la [directive 2013/55/UE](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=celex%3A32013L0055) du Parlement européen et du Conseil relative à la reconnaissance des qualifications professionnelles (« directive qualifications professionnelles »), mentionnées aux articles [R. 123-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000021926763), [R. 123-30-2](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000032941383&dateTexte=&categorieLien=cid) et [R. 123-30-9](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000034568806&dateTexte=&categorieLien=cid) du Code de commerce ;
* d’accéder, à distance et par voie électronique, à l’information sur ces formalités, procédures et exigences mentionnées aux articles [R. 123-2](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006255836&dateTexte=&categorieLien=cid), [R. 123-21](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006256030&dateTexte=&categorieLien=cid), [R. 123-30-2](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000032941383&dateTexte=&categorieLien=cid) et [R. 123-30-9](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000034568806&dateTexte=&categorieLien=cid) du Code du commerce.

Le site guichet-partenaires.fr est conçu et développé par le pôle « Guichet Entreprises » de l'INPI, en application du décret n° 2020-946 du 30 juillet 2020. Le pôle Guichet Entreprises gère les sites [guichet-entreprises.fr](https://www.guichet-entreprises.fr/fr/) et [guichet-qualifications.fr](https://www.guichet-qualifications.fr/fr/) qui constituent le guichet unique électronique défini par les directives européennes [2006/123/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/PDF/?uri=CELEX:32006L0123&from=EN) et [2005/36/CE](https://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2005:255:0022:0142:fr:PDF).

À l’INPI, nous avons pour ambition :

* de donner la possibilité aux futurs créateurs et aux dirigeants d’entreprise de réaliser, en ligne, les formalités administratives liées à la vie d’une entreprise ;
* d'encourager la mobilité professionnelle des résidents de l’Union européenne (UE) et de l’Espace économique européen (EEE) en offrant une information complète sur l’accès et l’exercice des professions réglementées en France, en vue d’une reconnaissance de qualification professionnelle.

Ainsi, sur guichet-partenaires.fr tout agent d'un organisme partenaire de l'INPI peut consulter, télécharger et archiver les dossiers dématérialisés de création, de modifications de la situation et de cessation d’activité d’une entreprise et de reconnaissance de qualification professionnelle dont il est destinataire.

Le site guichet-partenaires.fr s’adresse à tous les organismes partenaires du service Guichet Entreprises intervenant dans le parcours de création d'entreprise des citoyen français, des ressortissants de l'Union européenne (UE) ou de l'Espace économique européen (EEE) et dans la reconnaissance des qualifications professionnelles des ressortissants de l'UE et de l'EEE.

L’Institut national de la propriété industrielle
------------------------------------------------

Le pôle Guichet Entreprises est un pôle du service valorisation des données au sein de l’Institut national de la propriété, en application du décret n° 2020-946 du 30 juillet 2020. L’INPI est un établissement public placé sous la tutelle du ministère de l’Économie et des Finances. Créé en 1951, il est aujourd’hui présent sur 23 sites et emploie environ 780 personnes. 

### Missions

Fort de sa mission de soutien à l'innovation et à la compétitivité des entreprises, l'INPI accueille et informe tous ceux qui sont concernés par la propriété industrielle. Il les assiste tout au long de leurs démarches dans ce domaine, délivre les titres brevets, marques, dessins et modèles et en assure la publication.

* L'INPI traite chaque année environ 17 000 demandes de brevets, 90 000 demandes de marques et reçoit près de 6 000 demandes de dessins et modèles.
* L'INPI forme et sensibilise tous les acteurs économiques aux questions de la propriété industrielle.
* L'INPI délivre toute l'information nécessaire à la pratique de la propriété industrielle.
* L'INPI participe activement à la lutte contre la contrefaçon.
* L'INPI renforce l'influence de la France dans le domaine du droit de la propriété industrielle.

Pour en savoir plus sur nos missions, rendez-vous sur [inpi.fr](https://www.inpi.fr/fr).
