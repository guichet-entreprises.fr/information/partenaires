﻿<!-- include-file(gp.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     _____           _                   _               
|   / ____|     (_)    | |        | |   |  __ \         | |                 (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__) |_ _ _ __| |_ ___ _ __   __ _ _ _ __ ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  ___/ _` | '__| __/ _ \ '_ \ / _` | | '__/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |  | (_| | |  | ||  __/ | | | (_| | | | |  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |_|   \__,_|_|   \__\___|_| |_|\__,_|_|_|  \___||___/
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:breadcrumb)="Condition générales d'utilisation" -->
<!-- var(page:title)="Conditions générales d'utilisation" -->
<!-- var(collapsable)="close" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->

   

Conditions générales d'utilisation
==================================
Préambule <!-- collapsable:close -->
---------

Ce document présente les modalités d’engagement à l’utilisation du service en ligne Guichet Partenaires pour les partenaires. Il s’inscrit dans le cadre juridique :

* de la [directive 2006/123/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32006L0123&from=FR) du Parlement européen et du Conseil du 12 décembre 2006 relative aux services dans le marché intérieur ;
* du [règlement n° 910/2014](https://eur-lex.europa.eu/legal-content/FR/TXT/?uri=CELEX%3A32014R0910) du Parlement européen et du Conseil du 23 juillet 2014 (e-IDAS) sur l’identification électronique et les services de confiance pour les transactions électroniques au sein du marché intérieur ;
* du [règlement n° 2016/679](https://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32016R0679&from=FR) du Parlement européen et du Conseil du 27 avril 2016 relatif au traitement des données à caractère personnel ;
* de l’[ordonnance du 8 décembre 2005](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000636232) relative aux échanges électroniques entre les usagers et les autorités administratives et entre les autorités administratives et le décret n° 2010-112 du 2 février 2010 pris pour l’application des articles 9, 10 et 12 de cette ordonnance ;
* du [dispositif de la loi n° 78-17 du 6 janvier 1978 modifiée](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000886460) relative à l’informatique, aux fichiers et aux libertés ;
* de l’[article 441-1 du Code pénal](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006418753&cidTexte=LEGITEXT000006070719&dateTexte=20020101) du Code Pénal ;
* des articles R. 123-1 à R123-28 du Code de commerce ;
* du [décret n° 2020-946 du 30 juillet 2020](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000042182948&categorieLien=id) désignant l’Institut national de la propriété industrielle en tant qu’organisme unique mentionné au neuvième alinéa de l’article 1er de la loi n° 2019-486 du 22 mai 2019 relative à la croissance et la transformation des entreprises et confiant à cet institut la gestion des services informatiques mentionnés aux articles R. 123-21 et R. 123-30-9 du Code de commerce.

Objet du document <!-- collapsable:close -->
-----------------

Le présent document a pour objet de définir les conditions générales d’utilisation du service en ligne Guichet Partenaires, dénommé « service » ci-après, entre l'Institut national de la propriété industrielle (INPI) et les organismes partenaires.

Définition et objet du service <!-- collapsable:close -->
------------------------------

Le service mis en œuvre par l'INPI (ci-après dénommé « le service Guichet Partenaires ») contribue à simplifier les démarches liées à la création, aux modifications de la situation et à la cessation d’activité d’une entreprise des usagers français et européens.

L’utilisation du service est facultative et gratuite.

L’ensemble des destinataires du service sont dénommées ci-après les partenaires.

Fonctionnalités <!-- collapsable:close -->
---------------

Le service permet aux partenaires :

* d’accéder à la documentation précisant les fonctionnalités disponibles ;
* de créer un compte utilisateur donnant accès à un espace de stockage personnel. Cet espace permet aux partenaires de gérer et utiliser leurs données à caractère personnel, et de conserver les informations les concernant.

Depuis son espace, les usagers peuvent :

* visualiser, traiter, télécharger et archiver les dossiers des déclarants qui les concernent ;
* pour les référents des organismes partenaires, créer un nouvel utilisateur ;
* pour les référents des organismes partenaires, mettre à jour les informations de leur organisme ;
* pour les référents des organismes partenaires, configurer le canal de réception de leur organisme ;
* pour les référents paiement des organismes partenaires, mettre à jour les informations de paiement de leur organisme.

Modalités d’inscription et d’utilisation du service <!-- collapsable:close -->
---------------------------------------------------

L’accès au service est  gratuit et ouvert à toute personne autorisée. Il est facultatif et n’est pas exclusif d’autres canaux d’accès pour permettre au partenaire de recevoir les dossiers des déclarants qui le concernent.

Pour la gestion de l’accès à l’espace personnel du service, le partenaire partage les informations suivantes :

* l’adresse électronique du partenaire ;
* le mot de passe choisi par le partenaire.

Le traitement des données personnelles est décrit dans la [politique de protection des données](https://www.guichet-entreprises.fr/fr/politique-de-protection-des-donnees-personnelles/) du site [guichet.entreprises.fr](https://www.guichet-entreprises.fr/fr/).

L’utilisation du service requiert une connexion et un navigateur internet. Le navigateur doit être configuré pour autoriser les cookies de session.

Afin de garantir une expérience de navigation optimale, nous vous recommandons d’utiliser les versions de navigateurs suivantes :

* Firefox version 45 et plus ;
* Google Chrome version 48 et plus.

En effet, d'autres navigateurs sont susceptibles de ne pas supporter certaines fonctionnalités du service.

Le service est optimisé pour un affichage en 1024×768 pixels. Il est recommandé d’utiliser la dernière version du navigateur et de le mettre à jour régulièrement pour bénéficier des correctifs de sécurité et des meilleures performances.

Rôles et engagement <!-- collapsable:close -->
-------------------

### Engagement de l'INPI

 1. L'INPI met en œuvre et opère le service conformément au cadre juridique en vigueur défini en préambule.
 2. L'INPI s’engage à prendre toutes les mesures nécessaires permettant de garantir la sécurité et la confidentialité des informations fournies par l’usager.
 3. L'INPI s’engage à assurer la protection des données collectées dans le cadre du service, et notamment empêcher qu’elles soient déformées, endommagées ou que des tiers non autorisés y aient accès, conformément aux mesures prévues par l’ordonnance du 8 décembre 2005 relative aux échanges électroniques entre les usagers et les autorités administratives et entre les autorités administratives, le décret n° 2010-112 du 2 février 2010 pris pour l’application des articles 9, 10 et 12 de cette ordonnance et le règlement n° 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif au traitement des données à caractère personnel.
 4. L'INPI et les organismes partenaires garantissent aux usagers du service les droits d’accès, de rectification et d’opposition prévus par la loi n° 78-17 du 6 janvier 1978 relative à l’informatique aux fichiers et aux libertés et le réglement n° 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif au traitement des données à caractère personnel.<br/>
 Ce droit peut s’exercer de plusieurs façons :
  * en envoyant un courriel au [support](mailto:contact@guichet-partenaires.fr) ;<br/>
  * en envoyant un courrier à l'adresse suivante :
 
<p>&nbsp;&nbsp;&nbsp;Institut national de la propriété industrielle<br>
&nbsp;&nbsp;&nbsp;Pôle Guichet Entreprises<br>
&nbsp;&nbsp;&nbsp;15 rue des Minimes - CS50001<br>
&nbsp;&nbsp;&nbsp;92677 Courbevoie Cedex</p><br>

 5. L'INPI et les organismes partenaires s’engagent à n’opérer aucune commercialisation des informations et documents transmis par l’usager au moyen du service, et à ne pas les communiquer à des tiers, en dehors des cas prévus par la loi.
 6. L'INPI s’engage à assurer la traçabilité de toutes les actions réalisées par l’ensemble des utilisateurs du service.
 7. L'INPI offre aux usagers un support en cas d’incident ou d’alerte sécurité défini.

### Engagement du partenaire

Les présentes conditions générales s’imposent à tout utilisateur usager du service.

Disponibilité et évolution du service <!-- collapsable:close -->
-------------------------------------

Le service est disponible 7 jours sur 7, 24 heures sur 24. L'INPI se réserve toutefois la faculté de faire évoluer, de modifier ou de suspendre sans préavis le service pour des raisons de maintenance ou pour tout autre motif jugé nécessaire. L’indisponibilité du service ne donne droit à aucune indemnité. En cas d’indisponibilité du service, une page d’information est alors affichée au partenaire lui mentionnant cette indisponibilité ; il est alors invité à effectuer sa démarche ultérieurement.

Les termes des présentes conditions d’utilisation peuvent être amendés à tout moment, sans préavis, en fonction des modifications apportées au service, de l’évolution de la législation ou de la réglementation, ou pour tout autre motif jugé nécessaire.

Responsabilités <!-- collapsable:close -->
---------------

 1. La responsabilité de l'INPI ne peut être engagée en cas d’usurpation d’identité ou de toute utilisation frauduleuse du service.
 2. Le partenaire peut à tout moment modifier les données transmises à l'INPI ou les supprimer. Il peut choisir de supprimer toutes les informations de son compte en supprimant ses données auprès du service.