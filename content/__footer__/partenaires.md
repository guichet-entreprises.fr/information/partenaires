﻿<!-- include-file(gp.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     _____           _                   _               
|   / ____|     (_)    | |        | |   |  __ \         | |                 (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__) |_ _ _ __| |_ ___ _ __   __ _ _ _ __ ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  ___/ _` | '__| __/ _ \ '_ \ / _` | | '__/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |  | (_| | |  | ||  __/ | | | (_| | | | |  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |_|   \__,_|_|   \__\___|_| |_|\__,_|_|_|  \___||___/
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:title)="Nos partenaires" -->

Nos partenaires
===============

Le réseau
---------

Naturellement proches des enjeux des entrepreneurs dont ils sont les premiers interlocuteurs, les centres de formalités des entreprises (CFE) sont les partenaires privilégiés de l'Institut national de la propriété industrielle (INPI). Leur travail commun permet aujourd’hui aux entrepreneurs français et européens d'effectuer l'ensemble de leurs démarches facilement et rapidement, en ligne.

De nombreux acteurs sont aussi impliqués dans la reconnaissance des qualifications professionnelles des ressortissants de l'Union européenne et de l'Espace économique européen  désireux de travailler de manière temporaire ([libre prestation de services](https://www.guichet-entreprises.fr/fr/eugo/libre-prestation-de-services-lps/)) ou permanente ([libre établissement](https://www.guichet-entreprises.fr/fr/eugo/libre-etablissement-le/)) en France.

Nos partenaires (liste non exhaustive) :

* l'[Agence france entrepreneur (AFE)](https://www.afecreation.fr/) ;
* les [chambres d’agriculture (CA)](https://chambres-agriculture.fr/) ;
* les [chambres de commerce et d’industrie (CCI)](https://www.cci.fr/) ;
* les [chambres de métiers et de l’artisanat (CMA)](https://www.artisanat.fr/) ;
* les [greffiers des tribunaux de commerce](https://www.cngtc.fr/fr/) ;
* les [Urssaf](https://www.urssaf.fr/portail/home.html) ;
* les collectivités locales et territoriales ;
* les directions compétentes pour les activités et professions réglementées.

Les bureaux réglementaires
--------------------------

En France, certaines activités et professions sont réglementées. L'INPI publie des fiches d'information sur chacune d'entre elles : les fiches [Activités réglementées](https://www.guichet-entreprises.fr/fr/activites-reglementees/) sont disponibles sur le site [guichet-entreprises.fr](https://www.guichet-entreprises.fr/fr/), et les fiches [Professions réglementées](https://www.guichet-qualifications.fr/fr/professions-reglementees/) le sont sur [guichet-qualifications.fr](https://www.guichet-qualifications.fr/fr/).

Depuis la création des sites [guichet-entreprises.fr](https://www.guichet-entreprises.fr/fr/) et [guichet-qualifications.fr](https://www.guichet-qualifications.fr/), les bureaux réglementaires sont des partenaires précieux dans la mise à disposition du public d'une information à jour, fiable et structurée sur ces activités et professions réglementées.