﻿<!-- include-file(gp.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     _____           _                   _               
|   / ____|     (_)    | |        | |   |  __ \         | |                 (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__) |_ _ _ __| |_ ___ _ __   __ _ _ _ __ ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  ___/ _` | '__| __/ _ \ '_ \ / _` | | '__/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |  | (_| | |  | ||  __/ | | | (_| | | | |  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |_|   \__,_|_|   \__\___|_| |_|\__,_|_|_|  \___||___/
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:breadcrumb)="Mentions légales" -->
<!-- var(page:title)="Mentions légales" -->
<!-- var(collapsable)="close" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->

Mentions légales
================

Identification de l’éditeur <!-- collapsable:close --> 
---------------------------

<p>&nbsp;&nbsp;&nbsp;Institut national de la propriété industrielle<br>
&nbsp;&nbsp;&nbsp;15 rue des Minimes - CS50001<br>
&nbsp;&nbsp;&nbsp;92677 Courbevoie Cedex</p>

La conception éditoriale, le suivi, la maintenance technique et les mises à jour du site internet guichet-partenaires.fr sont assurés par le pôle Guichet Entreprises.

Le pôle Guichet Entreprises est un service de l'INPI, en application du décret n° 2020-946 du 30 juillet 2020.

Prestataire d’hébergement <!-- collapsable:close --> 
-------------------------

<p>&nbsp;&nbsp;&nbsp;Cloud Temple<br>
&nbsp;&nbsp;&nbsp;215 avenue Georges Clémenceau<br>
&nbsp;&nbsp;&nbsp;92024 Nanterre</p>
	
<p>&nbsp;&nbsp;&nbsp;Téléphone : 01 41 91 77 77</p>

Informatique et libertés <!-- collapsable:close --> 
------------------------

Vous disposez d’un droit d’accès et de rectification de vos données personnelles. En revanche, le traitement de celles-ci est nécessaire au respect d’une obligation légale à laquelle le responsable de traitement est soumis. Dès lors, le droit d’opposition à ce traitement n’est pas possible, en application des dispositions de l’article 6.1 c) du Règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016, et de l’article 56 de la loi n°78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés dans sa version consolidée.

Vous pouvez exercer ce droit de plusieurs façons :

* en envoyant un courriel à [cette adresse](contact@guichet-partenaires.fr) ;
* en envoyant un courrier à l'adresse suivante :

<p>&nbsp;&nbsp;&nbsp;Institut national de la propriété industrielle<br>
&nbsp;&nbsp;&nbsp;À l'attention du délégué à la protection des données<br>
&nbsp;&nbsp;&nbsp;15 rue des Minimes - CS50001<br>
&nbsp;&nbsp;&nbsp;92677 Courbevoie Cedex</p>

Droits de reproduction <!-- collapsable:close --> 
----------------------

Le contenu de ce site relève de la législation française et internationale sur le droit d’auteur et la propriété intellectuelle.

L’ensemble des éléments graphiques du site est la propriété de l'INPI. Toute reproduction ou adaptation des pages du site qui en reprendrait les éléments graphiques est strictement interdite.

Toute utilisation des contenus à des fins commerciales est également interdite.

Toute citation ou reprise de contenus du site doit avoir obtenu l’autorisation du directeur de la publication. La source (www.guichet-partenaires.fr) et la date de la copie devront être indiquées ainsi que la mention de l'Institut national de la propriété industrielle.

Liens vers les pages du site <!-- collapsable:close --> 
----------------------------

Tout site public ou privé est autorisé à établir des liens vers les pages du site guichet-partenaires.fr. Il n’y a pas à demander d’autorisation préalable. Cependant, l’origine des informations devra être précisée, par exemple sous la forme : « Création d’entreprise (source : www.guichet-partenaires.fr, un site de l'INPI) ». Les pages du site guichet-partenaires.fr ne devront pas être imbriquées à l’intérieur des pages d’un autre site. Elles devront être affichées dans une nouvelle fenêtre ou un nouvel onglet.

Liens vers les pages de sites extérieurs <!-- collapsable:close --> 
----------------------------------------

Les liens présents sur le site guichet-partenaires.fr peuvent orienter l’utilisateur sur des sites extérieurs dont le contenu ne peut en aucune manière engager la responsabilité de l'INPI.

Environnement technique <!-- collapsable:close --> 
-----------------------

Nous vous recommandons l'utilisation d'une version récente des navigateurs Mozilla Firefox et Google Chrome pour bénéficier de toutes les fonctionnalités du site Guichet Partenaires.