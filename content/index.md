<!-- include-file(gp.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     _____           _                   _               
|   / ____|     (_)    | |        | |   |  __ \         | |                 (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__) |_ _ _ __| |_ ___ _ __   __ _ _ _ __ ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  ___/ _` | '__| __/ _ \ '_ \ / _` | | '__/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |  | (_| | |  | ||  __/ | | | (_| | | | |  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |_|   \__,_|_|   \__\___|_| |_|\__,_|_|_|  \___||___/
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(site:home)="Accueil" -->
<!-- var(site:footer)="__footer__/__links__.md" -->
<!-- var(site:menu)="__menu__.md" -->
<!-- var(site:feedback_off)="off" -->
<!-- var(site:print_button_off)="off" -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:font_sizer_off)="off" -->
<!-- var(page:breadcrumb_off)="off" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:title)="Guichet Partenaires" -->

Guichet Partenaires  <!-- section-banner:arbres.jpg --><!-- color:dark -->
===================================================

En attendant le Guichet Unique, quelques précisions sur l’utilisation du Guichet Entreprises

[Lire l'article <!-- link-model:box-trans -->](articles/precisions_sur_guichet_entreprises.md)

Les espaces du Guichet Partenaires <!-- section-information:-100px -->
===================================

[Je m'informe](jeminforme/cadre_legal.html)
-----------------------------------------------------------------
Découvrez le Guichet Partenaires et son cadre légal, rencontrez votre équipe dédiée, etc.

[Je gère mes dossiers](https://account.guichet-partenaires.fr/session/new)
-----------------------------------------------------------------------
Connectez-vous, traitez vos dossiers, actualisez vos informations, etc.

Le service Guichet Entreprises en chiffres <!-- section-welcome: -->
====================================================================

Le service en ligne [guichet-entreprises.fr](https://www.guichet-entreprises.fr/fr/) encourage la création d’entreprise en France en permettant au citoyen de réaliser ses démarches administratives autour de la création d’une activité (immatriculation, demandes d’autorisation, etc.). Il est le site des pouvoirs publics de la création d’entreprise, de la modification et de la cessation d’activité d’une entreprise. Ce service est opéré par l'INPI.

Faciliter les démarches administratives, c’est encourager l’esprit d’entreprendre !

177436
-----
dossiers ont été constitués sur le site<br>[<font style="text-transform: lowercase;">guichet-entreprises.fr</font>](https://www.guichet-entreprises.fr/) en 2020.

44
-------
+44,24 % de dossiers ont été constitués sur le site<br>[<font style="text-transform: lowercase;">guichet-entreprises.fr</font>](https://www.guichet-entreprises.fr/) entre 2019 et 2020.

148357
-------
dossiers de création ont été constitués sur le site<br>[<font style="text-transform: lowercase;">guichet-entreprises.fr</font>](https://www.guichet-entreprises.fr/) en 2020.

40
-------
+40,5 % de dossiers de création ont été constitués sur le site<br>[<font style="text-transform: lowercase;">guichet-entreprises.fr</font>](https://www.guichet-entreprises.fr/) entre 2019 et 2020.

Des questions ?<!-- section:welcome --><!-- color:grey -->
=========================================

Vous avez des questions sur le fonctionnement du site ou des aspects techniques ? Vous n'avez pas reçu le courriel d'invitation ou le lien est inactif ? Nous vous invitons à [<font style="text-transform: lowercase;">consulter la</font> FAQ](faq.md) ou à [<font style="text-transform: lowercase;">contacter l'assistance utilisateur</font>](__footer__/contact.md).