﻿<!-- include-file(gp.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     _____           _                   _               
|   / ____|     (_)    | |        | |   |  __ \         | |                 (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__) |_ _ _ __| |_ ___ _ __   __ _ _ _ __ ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  ___/ _` | '__| __/ _ \ '_ \ / _` | | '__/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |  | (_| | |  | ||  __/ | | | (_| | | | |  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |_|   \__,_|_|   \__\___|_| |_|\__,_|_|_|  \___||___/
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:title)="Nos partenaires" -->

Nos partenaires
============================================

**Vous êtes teneur de registre, agent d'un Centre de Formalités des Entreprises (CFE) ou agent d'un organisme chargé de la reconnaissance des qualifications professionnelles ? Le site guichet-partenaires.fr est fait pour vous ! Sur guichet-partenaires.fr, vous avez accès à plusieurs fonctionnalités qui facilitent votre travail.**

Le réseau territorial
---------------------

Le site guichet-entreprises.fr est le fruit d’une démarche mise en œuvre à l’initiative de l’État français dans le cadre de l’application de la directive 2006/123/CE du Parlement européen et du Conseil du 12 décembre 2006 relative aux services dans le marché intérieur (« directive services).
Naturellement proches des enjeux des entrepreneurs dont ils sont les premiers interlocuteurs, les centres de formalités des entreprises (CFE) sont les partenaires privilégiés du pôle Guichet Entreprises de l'INPI. Leur travail permet aujourd’hui aux entrepreneurs français et européens d'effectuer l'ensemble de leurs démarches facilement et rapidement, en ligne.

De nombreux acteurs sont également impliqués dans la reconnaissance des qualifications professionnelles des ressortissants de l'Union européenne et de l'Espace économique européen désireux de travailler de manière temporaire (libre prestation de services) ou permanente (libre établissement) en France.

Nos partenaires (liste non exhaustive) :

* l'Agence France entrepreneur (AFE) ;
* les chambres d’agriculture (CA) ;
* les chambres de commerce et d’industrie (CCI) ;
* les chambres de métiers et de l’Artisanat (CMA) ;
* les greffiers des tribunaux de commerce ;
* les Urssaf ;
* les collectivités territoriales ;
* les directions compétentes pour les activités et professions réglementées.

Les bureaux réglementaires
-----------------------------------------

En France, certaines activités et professions sont réglementées. Le pôle Guichet Entreprises de l'INPI publie des fiches d'information sur chacune d'entre elles : 

-	Consultez les [fiches « Activités réglementées »](https://www.guichet-entreprises.fr/fr/activites-reglementees/) 
-	Consultez les [fiches « Professions réglementées »](https://www.guichet-qualifications.fr/fr/professions-reglementees/)

Les bureaux réglementaires sont des partenaires précieux dans la mise à disposition du public d'une information à jour, fiable et structurée sur ces activités et professions réglementées.