<!-- include-file(gp.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     _____           _                   _               
|   / ____|     (_)    | |        | |   |  __ \         | |                 (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__) |_ _ _ __| |_ ___ _ __   __ _ _ _ __ ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  ___/ _` | '__| __/ _ \ '_ \ / _` | | '__/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |  | (_| | |  | ||  __/ | | | (_| | | | |  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |_|   \__,_|_|   \__\___|_| |_|\__,_|_|_|  \___||___/
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:breadcrumb)="En attendant le Guichet Unique, quelques précisions sur l’utilisation du Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:title)="En attendant le Guichet Unique, quelques précisions sur l’utilisation du Guichet Entreprises" -->

En attendant le Guichet Unique, quelques précisions sur l’utilisation du Guichet Entreprises
==========================

Le Guichet Entreprises est un guichet électronique qui permet de dématérialiser les formalités administratives liées à la vie d’une entreprise tel que défini par les directives européennes 2006/123/CE et 2005/36/CE.

Quelques précisions sur l’usage du guichet-entreprises.fr :

- Le Guichet Entreprises n’a pas pour mission de traiter les formalités. Le Guichet Entreprises permet uniquement de simplifier l'envoi des documents au CFE compétent.
- Une fois le dossier transmis au CFE, le Guichet Entreprises est totalement dessaisi (article R.123-18 du code commerce). Dès lors, le CFE devient le seul interlocuteur du déclarant.
- Si le déclarant souhaite modifier son dossier, il ne peut pas le faire sur guichet-entreprises.fr. Il doit adresser sa demande à son CFE.
- Le Guichet Entreprises n’envoie pas de notifications au déclarant en cas de pièces manquantes ou de rejet du dossier. Seuls, les CFE ou les teneurs de registre sont habilités à le faire.

D’autres fonctionnalités seront développées ultérieurement par le Guichet Unique conçu par l’INPI dans le cadre de la loi Pacte.

