﻿<!-- include-file(gp.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     _____           _                   _               
|   / ____|     (_)    | |        | |   |  __ \         | |                 (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__) |_ _ _ __| |_ ___ _ __   __ _ _ _ __ ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  ___/ _` | '__| __/ _ \ '_ \ / _` | | '__/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |  | (_| | |  | ||  __/ | | | (_| | | | |  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |_|   \__,_|_|   \__\___|_| |_|\__,_|_|_|  \___||___/
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:title)="Connexion à votre espace dédié" -->

Connexion à votre espace dédié
==============================

**Simple, rapide et sécurisé, votre espace dédié sur guichet-partenaires.fr vous permet de recevoir, consulter en ligne, télécharger et archiver tous les dossiers déposés par les déclarants sur [guichet-entreprises.fr](https://www.guichet-entreprises.fr/fr/).**

Comment créer votre compte ? 
----------------------------

**Si vous êtes référent de votre réseau**, vous recevez un courriel contenant un lien vous permettant de créer puis de valider un compte sur guichet-partenaires.fr. Il vous suffit de suivre les étapes de la formalité de création de compte. Attention, ce courriel est valable 24h.

**Si vous êtes agent d'un organisme partenaire** du service Guichet Entreprises et que vous souhaitez instruire des dossiers depuis le site guichet-partenaires.fr, vous devez faire une demande de création de compte auprès de votre référent réseau, seule personne habilitée à effectuer cette action.

Comment accéder à votre espace ?
--------------------------------

Une fois votre compte créé, pour vous connecter à votre espace dédié :

1. Cliquez sur la flèche en haut de la page d’accueil de guichet-partenaires.fr à côté de « Compte » puis sur **« Connexion »**
<p align="center">![Connexion espace partenaire](../images/Connexion_espace.jpg)
<br>

2. Entrez votre courriel (1) et votre mot de passe puis cliquez sur **« Se connecter »**
<p align="center">![Saisie codes acces](../images/Saisie_acces.jpg)
<br>

(1) Il s’agit du courriel à partir duquel le compte a été créé.