﻿<!-- include-file(gp.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     _____           _                   _               
|   / ____|     (_)    | |        | |   |  __ \         | |                 (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__) |_ _ _ __| |_ ___ _ __   __ _ _ _ __ ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  ___/ _` | '__| __/ _ \ '_ \ / _` | | '__/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |  | (_| | |  | ||  __/ | | | (_| | | | |  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |_|   \__,_|_|   \__\___|_| |_|\__,_|_|_|  \___||___/
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:title)="Gestion de vos dossiers" -->

Gestion de vos dossiers
========================

**Votre espace dédié facilite votre travail et le traitement des dossiers dont vous êtes destinataire. Il vous permet d'avoir une vision globale des dossiers que vous avez traités et/ou qu'il vous reste à traiter.**

Comment accéder à mes dossiers ? 
----------------------------

Une fois votre compte créé sur guichet-partenaires.fr, vous êtes redirigé, après authentification, sur votre espace partenaire. 
Celui-ci se présente sous la forme d’un tableau de bord :

<p align="center">![Tableau de bord partenaires](../images/tableau_bord.jpg)
<br>

Il regroupe :
* l’ensemble des dossiers que vous avez traités ou qu’il vous reste à traiter ;
* une barre de recherche vous permettant de trouver rapidement un dossier par nom/dénomination, date de transmission, type de formalité ou numéro de liasse ; 
* un bouton vous permettant de les trier par date de transmission ; 
* un bouton « Les services disponibles » vous permettant d’accéder à des fonctionnalités telles que la mise à jour des informations de votre autorité ou la création d’un nouvel utilisateur. 

Ces fonctionnalités varient selon votre profil (utilisateur, référent).

Comment traiter mes dossiers ?
--------------------------------

Sur votre espace partenaire, chaque dossier comporte les informations suivantes : 

* la référence utilisateur ou *user ID*, interne au pôle Guichet Entreprises de l'INPI ;
* la date de transmission du dossier ; 
* le numéro de liasse en H10000. Cette référence est nécessaire à l’ensemble de nos échanges (sauf pour les activités agricoles) ; 
* la dénomination ou le nom et le prénom du déclarant dans le cas d’une personne physique ; 
* le type de formalité ; 
* l’état du dossier (« A traiter » ou « Archivé »).

Enfin, trois boutons vous permettent les actions suivantes :

![dossiers](../images/dossier.jpg)

  - **Consulter :** visualiser les données principales du dossier puis consulter et/ou imprimer le Cerfa et les pièces justificatives.
  - **Archiver :** archiver les dossiers que vous avez déjà traités. Attention, cette action est irréversible !
  - **Télécharger :**  télécharger le dossier au format ZIP.

Si votre référent a activé l’option « Notification », vous pouvez être averti de l’arrivée d’un dossier par un courriel. Vous accédez alors directement au dossier concerné en cliquant sur le lien.