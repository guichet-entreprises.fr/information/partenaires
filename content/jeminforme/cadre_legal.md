﻿<!-- include-file(gp.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     _____           _                   _               
|   / ____|     (_)    | |        | |   |  __ \         | |                 (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__) |_ _ _ __| |_ ___ _ __   __ _ _ _ __ ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  ___/ _` | '__| __/ _ \ '_ \ / _` | | '__/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |  | (_| | |  | ||  __/ | | | (_| | | | |  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |_|   \__,_|_|   \__\___|_| |_|\__,_|_|_|  \___||___/
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:title)="Cadre légal" -->

Cadre légal
==================

Le site guichet-partenaires.fr est un site géré par le pôle Guichet Entreprises de l'INPI, en application du [décret n° 2020-946](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000042182948/) du 30 juillet 2020.

L'INPI a pour mission d’assurer la mise à disposition d’un service électronique accessible par l’internet, sécurisé et gratuit, permettant :

* d’accomplir, à distance et par voie électronique, les formalités nécessaires à la création, aux modifications de situation et à la cessation d’activité d’une entreprise ainsi qu’à l’accès à une activité réglementée et à son exercice, au sens de la directive [2006/123/CE](https://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32006L0123&from=FR) du Parlement européen et du Conseil du 12 décembre 2006 relative aux services dans le marché intérieur (« directive services »), mentionnées aux articles R. 123-1 et R. 123-30-2 du Code de commerce ;
* d’accomplir, à distance et par voie électronique, les formalités, procédures et exigences en matière de reconnaissance, pour l’exercice d’une profession réglementée en France, des qualifications professionnelles acquises dans un autre État membre de l’Union européenne ou de l’Espace économique européen, au sens de la directive [2013/55/UE](https://eur-lex.europa.eu/legal-content/FR/TXT/HTML/?uri=CELEX:32013L0055&from=FR) du Parlement européen et du Conseil relative à la reconnaissance des qualifications professionnelles (« directive qualifications professionnelles »), mentionnées aux articles R. 123-1, R. 123-30-2 et R. 123-30-9 du Code de commerce ;
* d’accéder, à distance et par voie électronique, à l’information sur ces formalités, procédures et exigences mentionnées aux articles R. 123-2, R. 123-21, R. 123-30-2 et R. 123-30-9 du Code du commerce.

Les organismes destinataires des dossiers conformes aux articles [R. 123-7](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006255891&dateTexte=&categorieLien=cid), [R. 123-23](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000032944474&dateTexte=&categorieLien=id) et [R. 123-24](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=291827F72B9E2E1DCEA84F4A9A285CDD.tplgfr42s_2?idArticle=LEGIARTI000035680237&cidTexte=LEGITEXT000005634379&dateTexte=20180927&categorieLien=id&oldAction=&nbResultRech=) du Code de commerce déposés par les déclarants sur les sites [guichet-entreprises.fr](https://www.guichet-entreprises.fr/) et [guichet-qualifications.fr](https://www.guichet-qualifications.fr/fr/) sont responsables de la transmission aux organismes et administrations destinataires des éléments du dossier de déclaration d'entreprise qu'ils ont reçus par voie électronique conformément à l'article [R. 123-25](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000005634379&idArticle=LEGIARTI000006256049&dateTexte=&categorieLien=cid) du Code de commerce.