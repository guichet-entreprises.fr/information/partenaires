﻿<!-- include-file(gp.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     _____           _                   _               
|   / ____|     (_)    | |        | |   |  __ \         | |                 (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__) |_ _ _ __| |_ ___ _ __   __ _ _ _ __ ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  ___/ _` | '__| __/ _ \ '_ \ / _` | | '__/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |  | (_| | |  | ||  __/ | | | (_| | | | |  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |_|   \__,_|_|   \__\___|_| |_|\__,_|_|_|  \___||___/
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante:
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:title)="Un outil dédié à la gestion de vos dossiers" -->

Un outil dédié à la gestion de vos dossiers
============================================

**Vous êtes teneur de registre, agent d'un Centre de Formalités des Entreprises (CFE) ou agent d'un organisme chargé de la reconnaissance des qualifications professionnelles ? Le site guichet-partenaires.fr est fait pour vous ! Sur guichet-partenaires.fr, vous avez accès à plusieurs fonctionnalités qui facilitent votre travail.**

Quelles sont les fonctionnalités offertes par le site ?
-------------------------------------------------------

Selon votre profil, vous avez accès à différentes fonctionnalités. 

La rubrique « S’informer » est, quant à elle, ouverte à tous.

### S'informer

Connaître le cadre légal, consulter la FAQ, contacter le service Guichet Entreprises.

### Instruire

Accéder aux dossiers des déclarants, les télécharger, les instruire et les archiver.

### Gérer

Créer un nouvel utilisateur et paramétrer votre organisation.

### Paramétrer

Modifier vos paramètres de réception des dossiers, vos coordonnées de transmission, etc.

Comment sont définis les rôles accordés ?
-----------------------------------------

Un référent est identifié par le pôle Guichet Entreprises de l'INPI ou par la tête de réseau au sein de chaque entité qui souhaite profiter des services du site guichet-partenaires.fr. Une fois son compte ouvert, le référent attribue un rôle spécifique à chacun de ses agents en fonction de leur mission au sein du CFE.

Il peut, par exemple, créer des comptes « utilisateur » et « référent » pour les agents de son organisme.

A quoi a-t-on accès en fonction du rôle qui nous a été assigné ?
----------------------------------------------------------------

Si vous êtes « référent », vous pouvez:
* accéder aux dossiers des déclarants adressés à votre entité et les télécharger ;
* les trier (par date de transmission ou par ordre alphabétique) ;
* les archiver ;
* créer un nouveau compte pour vos agents ;
* nous contacter ;
* configurer vos canaux de transmission (dossiers dématérialisés transmis par courriel, le canal FTP, la fonction back office, ou en version papier) ; 
* mettre à jour les informations de contact de votre autorité.

Si vous êtes désigné « utilisateur » par votre référent réseau, vous pouvez: 
* accéder aux dossiers des déclarants et les télécharger ;
* les trier (par date de transmission ou par ordre alphabétique) ;
* les archiver ;
* nous contacter.

Chaque utilisateur a ainsi une visibilité complète sur l’ensemble des dossiers que son organisme a traité et ceux qu'il lui reste à traiter. Il peut ainsi administrer et organiser le traitement des dossiers à destination de son organisme selon ses besoins.