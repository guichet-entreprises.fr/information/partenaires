﻿<!-- include-file(gp.txt)
+-----------------------------------------------------------------------------+
|    _____       _      _          _     _____           _                   _               
|   / ____|     (_)    | |        | |   |  __ \         | |                 (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__) |_ _ _ __| |_ ___ _ __   __ _ _ _ __ ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  ___/ _` | '__| __/ _ \ '_ \ / _` | | '__/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |  | (_| | |  | ||  __/ | | | (_| | | | |  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |_|   \__,_|_|   \__\___|_| |_|\__,_|_|_|  \___||___/
| 
| 
+-------------------------------------------------------------------------- -->
<!-- include-file(license-short.txt)
+-----------------------------------------------------------------------------+
| Le référentiel d'information de Guichet Entreprises est mis à disposition
| selon les termes de la licence Creative Commons Attribution - Pas de
| Modification 4.0 International.
| 
| Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
| suivante :
| http://creativecommons.org/licenses/by-nd/4.0/
| ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
| Mountain View, California, 94041, USA.
+-------------------------------------------------------------------------- -->
<!-- var(page:author)="Guichet Entreprises" -->
<!-- var(page:description)="" -->
<!-- var(page:keywords)="" -->
<!-- var(page:title)="Foire aux questions " -->
<!-- var(collapsable)="close" -->

Foire aux questions 
===================

## Mon compte

### J'ai oublié mon mot de passe <!-- collapsable:close -->

Si vous avez oublié votre mot de passe, veuillez [le renouveler](https://account.guichet-partenaires.fr/users/renew). Saisissez l'adresse courriel que vous utilisez sur guichet-partenaires.fr et vous recevrez par courriel un lien à usage unique qui vous permettra de renouveler votre mot de passe. Le lien restera valable pendant 24 heures.

![Renouvellement du mot de passe <!-- align:center -->](images/faq_renouvellement_mot_de_passe.png)

### Puis-je modifier mes informations de contact ? <!-- collapsable:close -->

Vous pouvez accéder à votre compte depuis votre espace partenaire **en cliquant sur le bouton « Modifier mon compte »** en haut à droite de la page.

![Modifier le compte <!-- align:center -->](images/faq_modifier_compte.png)

Vous aurez alors la possibilité de changer vos informations personnelles.

![Informations du compte <!-- align:center -->](images/faq_informations_compte.png)

Pour modifier votre mot de passe, veuillez [suivre la procédure d’oubli de mot de passe <!-- toggle:env -->](https://account.guichet-partenaires.fr/users/renew).

### Mon référent m'a créé un compte mais je ne peux pas me connecter <!-- collapsable:close -->

Si vous ne pouvez pas accéder au compte créé par votre référent, veuillez [renouveler votre mot de passe <!-- toggle:env -->](https://account.guichet-partenaires.fr/users/renew). Si malgré cela vous ne parvenez toujours pas à vous connecter, [veuillez contacter l'assistance utilisateur](__footer__/contact.md).

### Je suis référent mais je n'ai pas reçu le courriel d'activation <!-- collapsable:close -->

Si vous n'avez pas reçu la courriel d'activation, [veuillez contacter l'assistance utilisateur](__footer__/contact.md).

## Mon espace partenaire

### Je dispose d'un compte sur guichet-partenaires.fr mais je ne vois pas mes dossiers sur mon espace partenaire <!-- collapsable:close -->

Si vous ne pouvez pas accéder aux dossiers de votre compte, [veuillez contacter l'assistance utilisateur](__footer__/contact.md).

### Je suis tête de réseau mais je ne peux pas visualiser la liste des agents de mon réseau <!-- collapsable:close -->

Si vous ne pouvez pas visualiser la liste des agents de votre réseau, [veuillez contacter l'assistance utilisateur](__footer__/contact.md).

### Je ne parviens pas à visualiser les fichiers PDF de mes dossiers <!-- collapsable:close -->

Si vous ne parvenez pas à visualiser les fichiers PDF de vos dossiers depuis un navigateur autre que Mozilla Firefox ou Google Chrome, nous vous recommandons d'utiliser une version récente de l'un de ces deux navigateurs. Si malgré cela vous ne parvenez toujours pas à les visualiser, [veuillez contacter l'assistance utilisateur](__footer__/contact.md).