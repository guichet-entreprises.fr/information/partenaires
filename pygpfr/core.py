#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
#   All Rights Reserved.
#   Unauthorized copying of this file, via any medium is strictly prohibited
#   Dissemination of this information or reproduction of this material
#   is strictly forbidden unless prior written permission is obtained
#   from Guichet Entreprises.
###############################################################################

###############################################################################
# Common functions for the content managment.
###############################################################################
import logging
import sys
import os
import os.path
import pymdtools.common
from pymdtools.mdfile import MarkdownContent

# -----------------------------------------------------------------------------
# check and change the include file in the header
#
# @return the MD.
# -----------------------------------------------------------------------------
def check_newline(md_content):
    result = md_content.content
    result = result.replace("\r\n", "\n")
    result = result.replace("\n\n\n", "\n\n")
    result = result.replace("\n\n\n\n", "\n\n")
    result = result.replace("\n\n\n\n\n", "\n\n")
    result = result.replace("->\n\n<!-", "->\n<!-")
    result = result.replace("->\n\n\n<!-", "->\n<!-")
    result = result.replace("->\n\n\n\n<!-", "->\n<!-")
    result = result.replace("->\n\n\n\n\n<!-", "->\n<!-")
    result = result.replace("->\n\n\n\n\n\n<!-", "->\n<!-")
    result = result.replace("->\n\n\n\n\n\n\n<!-", "->\n<!-")

    md_content.content = result.strip()

    return md_content

###############################################################################
# Process tags in md
###############################################################################
def update_md(md_content):
    md_content.process_tags()

###############################################################################
# check and change var in md
###############################################################################
def var_gp(md_content):
    md_content.del_include_file("ge.txt")
    md_content.del_include_file("gq.txt")
    md_content.del_include_file("license.txt")
    md_content.del_include_file("license.en.txt")
    md_content.del_include_file("license-short.txt")
    md_content.set_include_file("gp.txt")
    md_content.set_include_file("license-short.txt")

    target_value = {
        'page:description': "",
        'page:keywords': "",
        'page:author': "Guichet Entreprises",
        'page:title': md_content.title,
    }

    for var in target_value:
        if var not in md_content:
            print("Add the key=%s: value=%s  " % (var, target_value[var]))
            md_content[var] = target_value[var]
            continue

        if md_content[var] == target_value[var]:
            continue

        print("Problem with the key=%s: value=%s  " % (var, md_content[var]))
        print("                 --> should be=%s  " % target_value[var])
        md_content[var] = target_value[var]

    deleted_key = ['author', 'title', 'description', 'keywords']

    for key in deleted_key:
        del md_content[key]

    return md_content

###############################################################################
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the folder of THIS script.
###############################################################################
def process_gp_content(process):
    include_folder = os.path.join(__get_this_folder(), "..", "content_include")
    content_folder = os.path.join(__get_this_folder(), "..", "content")
    content_folder = pymdtools.common.check_folder(content_folder)
    logging.info("Content folder %s", content_folder)

    list_processes = []
    if isinstance(process, list):
        list_processes.extend(process)
    else:
        list_processes.append(process)

    def joined_processes(filename):
        logging.info("Update %s", filename)
        md_content = MarkdownContent(filename, 
                                     search_folders=[include_folder])
        for process in list_processes:
            process(md_content)

        md_content.backup=False
        md_content.write()

    pymdtools.common.apply_function_in_folder(content_folder,
                                              joined_processes)


###############################################################################
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the folder of THIS script.
###############################################################################
def __get_this_folder():
    return os.path.split(os.path.abspath(os.path.realpath(
        __get_this_filename())))[0]


###############################################################################
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the filename of THIS script.
###############################################################################
def __get_this_filename():
    result = ""

    if getattr(sys, 'frozen', False):
        # frozen
        result = sys.executable
    else:
        # unfrozen
        result = __file__

    return result

###############################################################################
# Set up the logging system
###############################################################################
def __set_logging_system():
    log_filename = os.path.splitext(os.path.abspath(
        os.path.realpath(__get_this_filename())))[0] + '.log'
    logging.basicConfig(filename=log_filename, level=logging.DEBUG,
                        format='%(asctime)s: %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(asctime)s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

###############################################################################
# Main script call only if this script is runned directly
###############################################################################
def __main():
    # ------------------------------------
    logging.info('Started %s', __get_this_filename())
    logging.info('The Python version is %s.%s.%s',
                 sys.version_info[0], sys.version_info[1], sys.version_info[2])

    fun_list = []
    fun_list.append(update_md)
    fun_list.append(check_newline)
    # fun_list.append(var_gp)
    process_gp_content(fun_list)

    logging.info('Finished')
    # ------------------------------------


###############################################################################
# Call main function if the script is main
# Exec only if this script is runned directly
###############################################################################
if __name__ == '__main__':
    __set_logging_system()
    __main()
