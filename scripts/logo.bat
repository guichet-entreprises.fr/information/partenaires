@ECHO off
REM # -----------------------------------------------------------------------------
REM # 
REM # Le référentiel d'information de Guichet Entreprises est mis à disposition
REM # selon les termes de la licence Creative Commons Attribution - Pas de
REM # Modification 4.0 International.
REM #
REM # Pour accéder à une copie de cette licence, merci de vous rendre à l'adresse
REM # suivante :
REM # http://creativecommons.org/licenses/by-nd/4.0/
REM # ou envoyez un courrier à Creative Commons, 444 Castro Street, Suite 900,
REM # Mountain View, California, 94041, USA.
REM # 
REM # -----------------------------------------------------------------------------
CALL %*
GOTO EOF
REM -------------------------------------------------------------------------------
:PRINT_LOGO
(
    SET "COMMON=%~dp0/common.bat"
    SETLOCAL EnableDelayedExpansion
    CALL !COMMON! :PRINT_LINE "╔══════════════════════════════════════════════════════════════════════════════════════════════════╗"
    CALL !COMMON! :PRINT_LINE "║                        _____       _      _          _                                           ║"
    CALL !COMMON! :PRINT_LINE "║                       / ____|     (_)    | |        | |                                          ║"
    CALL !COMMON! :PRINT_LINE "║                      | |  __ _   _ _  ___| |__   ___| |_                                         ║"
    CALL !COMMON! :PRINT_LINE "║                      | | |_ | | | | |/ __| '_ \ / _ \ __|                                        ║"
    CALL !COMMON! :PRINT_LINE "║                      | |__| | |_| | | (__| | | |  __/ |_                                         ║"
    CALL !COMMON! :PRINT_LINE "║                       \_____|\__,_|_|\___|_| |_|\___|\__|                                        ║"
    CALL !COMMON! :PRINT_LINE "║                       _____           _                   _                                      ║"
    CALL !COMMON! :PRINT_LINE "║                      |  __ \         | |                 (_)                                     ║"
    CALL !COMMON! :PRINT_LINE "║                      | |__) |_ _ _ __| |_ ___ _ __   __ _ _ _ __ ___  ___                        ║"
    CALL !COMMON! :PRINT_LINE "║                      |  ___/ _` | '__| __/ _ \ '_ \ / _` | | '__/ _ \/ __|                       ║"
    CALL !COMMON! :PRINT_LINE "║                      | |  | (_| | |  | ||  __/ | | | (_| | | | |  __/\__ \                       ║"
    CALL !COMMON! :PRINT_LINE "║                      |_|   \__,_|_|   \__\___|_| |_|\__,_|_|_|  \___||___/                       ║"
    CALL !COMMON! :PRINT_LINE "║                                                                                                  ║"
    CALL !COMMON! :PRINT_LINE "╚══════════════════════════════════════════════════════════════════════════════════════════════════╝"
    ENDLOCAL
    exit /b
)
:EOF